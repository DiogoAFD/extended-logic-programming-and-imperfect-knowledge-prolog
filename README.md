# **Health Institutions** #
## **Extended Logic Programming and Imperfect Knowledge** ##

The project intends to record events in health institutions, using logic programming, specifically Prolog language, SICStus 4.3.0 version. For such this application was developed in Java and taking advantage of JASPER library, which allows the connection between the SICStus and Java.
The application has a SWING GUI.